#!/bin/sh

#SBATCH --job-name=QC.SCREEN

# job submit as
# sbatch --cpus-per-task=12 rm_adapter.sh

PROJ_DIR=$HOME/SCH10113_Samya
mkdir -p $PROJ_DIR/test_samples_merged/{screen,screen_multiqc,fastqc,fastqc_multiqc}

SEQ_DIR=$PROJ_DIR/test_samples_merged

mkdir -p $PROJ_DIR/test_samples_clean
CLEAN_DIR=$PROJ_DIR/test_samples_clean 

mkdir -p $CLEAN_DIR/{fastq,adapter_matched,screen,screen_multiqc,fastqc,fastqc_multiqc}
FASTQ_DIR=$CLEAN_DIR/fastq

module load bowtie2/2.2.6
module load FastQC/0.11.4

# GG31-JF174_S3_R1_001.fastq.gz

# get sample IDs from file names
list=$(ls $SEQ_DIR/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_R"}{ print $1 }' | sort | uniq)
echo $list

for file in $list; do
 echo "ID --> ${file}"
  # 1. Clean
  # Usage:  bbduk.sh in=<input file> out=<output file> ref=<contaminant files>
  # k=27 (default)
  # 
 ~/bbmap/bbduk.sh \
  in=$SEQ_DIR/${file}_R1_001.fastq.gz in2=$SEQ_DIR/${file}_R2_001.fastq.gz \
  out=$FASTQ_DIR/${file}_R1_001.fastq.gz out2=$FASTQ_DIR/${file}_R2_001.fastq.gz \
  outm1=$CLEAN_DIR/adapter_matched/${file}_R1_matched.fastq.gz \
  outm2=$CLEAN_DIR/adapter_matched/${file}_R2_matched.fastq.gz \
  stats=$CLEAN_DIR/adapter_matched/${file}_stats.txt \
  ktrim=r k=23 mink=11 hdist=1 tpe tbo \
  ref=~/bbmap/resources/adapters.fa

  # 2. Fastq_Screen
  #
 for j in 1 2; do
  R=$SEQ_DIR/${file}_R${j}_001.fastq.gz  
  fastq_screen \
   --aligner bowtie2 \
   -conf /home/adinasarapu/fastq_screen_v0.13.0/fastq_screen.conf \
   --subset 100000 \
   --outdir $SEQ_DIR/screen \
   $R

  R_clean=$FASTQ_DIR/${file}_R${j}_001.fastq.gz
  fastq_screen \
   --aligner bowtie2 \
   -conf /home/adinasarapu/fastq_screen_v0.13.0/fastq_screen.conf \
   --subset 100000 \
   --outdir $CLEAN_DIR/screen \
   $R_clean
 done
done

# 3. MultiQC - screen 
multiqc $SEQ_DIR/screen --outdir $SEQ_DIR/screen_multiqc
multiqc $CLEAN_DIR/screen --outdir $CLEAN_DIR/screen_multiqc

# 4. FastQC
fastqc -t 10 $SEQ_DIR/*.fastq.gz -o $SEQ_DIR/fastqc
fastqc -t 10 $FASTQ_DIR/*.fastq.gz -o $CLEAN_DIR/fastqc

# 5. MultiQC - fastqc
multiqc $SEQ_DIR/fastqc --outdir $SEQ_DIR/fastqc_multiqc
multiqc $CLEAN_DIR/fastqc --outdir $CLEAN_DIR/fastqc_multiqc

module unload FastQC/0.11.4
module unload bowtie2/2.2.6
