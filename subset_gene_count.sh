#!/bin/sh

# run this script as
# sh ...
PROJ_DIR=$HOME/SCH10113_Samya/targetedRNA/Count
GENES=$HOME/SCH10113_Samya/rnaseq-nmd/data/GENES.txt

OUT=$PROJ_DIR/ReadsPerGene_NMD274
if [ ! -d $OUT ]; then
 mkdir -p $OUT
fi

list=$(ls $PROJ_DIR/ReadsPerGene/* | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' 2>/dev/null)
# list="10215 110442 40107 40185-1"

echo $list

COUNTER=0

for file in $list; do
  COUNTER=$(( $COUNTER + 1 ))
  echo "${COUNTER}. Extracting 274 NMD Genes from ${file}_ReadsPerGene.out.tab"
  grep -w -F -f $GENES -e N_unmapped -e N_multimapping -e N_noFeature -e N_ambiguous $PROJ_DIR/ReadsPerGene/${file}_ReadsPerGene.out.tab | awk -F '\t' '{print $1,$2,$3,$4}' > $OUT/${file}_ReadsPerGene.out.tab
done
