#!/bin/sh

PROJ_DIR=$HOME/SCH10113_Samya/targetedRNA/Count
IN_DIR=$PROJ_DIR/ReadsPerGene_NMD274
OUT=$PROJ_DIR/ReadsPerGene_NMD274_merged
if [ ! -d $OUT ]; then
 mkdir -p $OUT
fi

# ReadsPerGene.out.tab
# column 1: gene ID
# column 2: counts for unstranded RNA-seq
# column 3: counts for the 1st read strand aligned with RNA (htseq-count option -s yes) 
# column 4: counts for the 2nd read strand aligned with RNA (htseq-count option -s reverse)

#files=$(ls -d $IN_DIR/* 2>/dev/null)
files=$(ls $IN_DIR/* | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' 2>/dev/null)

cd $IN_DIR

echo $files
arr=($files)
arr_len=${#arr[@]}

colnames="id"

file1=${arr[0]}
colnames+="\t$file1"

file2=${arr[1]}
colnames+="\t$file2"

# Note that the array indices are counted 0,1,..
# join file1 file2 | join - file3 > output
cat ${arr[0]}_ReadsPerGene.out.tab | awk -v OFS='\t' '{print $1, $2}' > ${arr[0]}_ReadsPerGene.out.tab.mod
# sort -o ${arr[0]}/ReadsPerGene.out.tab.mod

cat ${arr[1]}_ReadsPerGene.out.tab | awk -v OFS='\t' '{print $1, $2}' > ${arr[1]}_ReadsPerGene.out.tab.mod
# sort -o ${arr[1]}/ReadsPerGene.out.tab.mod 

run_cmd="join -t $'\t' ${arr[0]}_ReadsPerGene.out.tab.mod ${arr[1]}_ReadsPerGene.out.tab.mod"

for (( i=2; i<$arr_len; i++ )); do

  cat ${arr[$i]}_ReadsPerGene.out.tab | awk -v OFS='\t' '{print $1, $2}' > ${arr[$i]}_ReadsPerGene.out.tab.mod
  # sort -o ${arr[$i]}/ReadsPerGene.out.tab.mod

  run_cmd+=" | join - -t $'\t' ${arr[$i]}_ReadsPerGene.out.tab.mod"
  #eval $run_cmd
  filex=${arr[$i]}
  colnames+="\t$filex"

done

cmd="$run_cmd > $OUT/CountMerged1.txt"
eval $cmd

# add header
echo -e $colnames | cat - $OUT/CountMerged1.txt > $OUT/CountMerged_unfiltered.txt

# remove rows with all zeros
awk 'FNR == 1{print;next}{for(i=2;i<=NF;i++) if($i > 0){print;next}}' $OUT/CountMerged_unfiltered.txt > $OUT/CountMerged_filtered.txt
rm $OUT/CountMerged1.txt
