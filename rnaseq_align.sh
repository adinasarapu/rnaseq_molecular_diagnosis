#!/bin/bash
scriptname=$0   # $0 is the name of the program
seqtype=targeted #either whole or targeted
batch=""
trim=no
kmersize=35
readLength=151
vars=$HOME/SCH10113_Samya/rnaseq-nmd/scripts/variableslist.sh
###
#help function
HELP () {
	echo -e "\n\tUSAGE: $scriptname -b BATCH [-s (whole|targeted)] [-t (yes|no)] [-v VARIABLES_FILE]\n"
	echo -e "\tMANDATORY OPTIONS:\n"
	echo -e "\t\t-b BATCH\t\tName of the sequencing run, usually in the form of mmmYYYY\n\n"
	echo -e "\tADDITIONAL OPTIONS:\n"
	echo -e "\t\t-v VARIABLES_FILE\tLocation of file containing variable information for analysis scripts"
	echo -e "\t\t\t\t\tDEFAULT: ~/data/scripts/variableslist.sh\n"	
	echo -e "\t\t-s SEQTYPE\t\tType of RNA sequenced"  
	echo -e "\t\t\t\t\tDEFAULT: targeted\n"						
	echo -e "\t\t-t TRIM\t\t\tWhether the FASTQ files to be aligned have been trimmed"
	echo -e "\t\t\t\t\tDEFAULT: no\n"
	echo -e "\t\t-k KMER_SIZE\t\tkmer size to use in GATK HaplotypeCaller. Must be an integer."
	echo -e "\t\t\t\t\tDEFAULT: 35\n"
	echo -e "\t\t-l READ_LENGTH\t\tRead Length. Must be an integer."
	echo -e "\t\t\t\t\tDEFAULT: 151\n"
	exit 0
}

#0. <script_file>
#1. batch = 2019
#2. targeted = 

###
#Check the number of arguments. If none are passed, print help and exit.
NUMARGS=$#
if [ $NUMARGS -eq 0 ]; then
  HELP
fi

###
#Get Options from command line
while getopts :k:t:l:b:s:v:h opt
do
	case $opt in
	t)trim=$OPTARG;;
	b)batch=$OPTARG;;
	s)seqtype=$OPTARG;;
	k)kmersize=$OPTARG;;
	l)readLength=$OPTARG;;
	v)vars=$OPTARG;;
    h) HELP; exit;;
    \?) echo "ERROR: Invalid option: -$OPTARG" >&2; echo -e "Run $scriptname -h to see usage and help" >&2; exit 1;;
    :) echo "ERROR: Option -$OPTARG requires an argument" >&2; echo -e "Run $scriptname -h to see usage and help" >&2; exit 1;;
    *) echo "ERROR: Unimplemented option: -$OPTARG" >&2; echo -e "Run $scriptname -h to see usage and help" >&2; exit 1;;
  esac
done
shift $(($OPTIND -1))
###

#check to make sure variables file exists
if [ ! -f $vars ]; then
  echo -e "ERROR: File $vars DOES NOT exist" >&2
  echo -e "Edit this scripts default or use the -v option to point the script to your file that contains variables for analysis scripts" >&2
  exit 1
fi

###
source $vars

###
# check if seqtype is whole or targeted, set seqfile to appropriate directory name
seqfile=""
if [ $seqtype == 'targeted' ]; then 
  seqfile=targetedRNA
elif [ $seqtype == 'whole' ]; then
  seqfile=wholemRNA
else
  echo -e "ERROR: \"$seqtype\" is not a valid argument for [-s] option. Valid arguments are \"targeted\" or \"whole\""
  exit 1
fi

# make sure kmer size is a positive integer
if ! [[ "$kmersize" =~ ^[0-9]+$ ]]; then
  echo -e "ERROR: \"$kmersize\" is not a valid argument for [-k] option. kmer size must be an integer."
  exit 1
fi

if ! [[ "$readLength" =~ ^[0-9]+$ ]]; then
  echo -e "ERROR: \"$readLength\" is not a valid argument for [-l] option. Read Length must be an integer."
  exit 1
fi

# check whether or not to trim, set fastq_dir appropriately
fastq_type=""
if [ $trim == 'yes' ]; then 
  fastq_type=fastq_files/trimmed
  echo "Will align TRIMMED fastq files"
elif [ $trim == 'no' ]; then
  fastq_type=fastq_files
  echo "Will align UNTRIMMED fastq files"
else
  echo -e "ERROR: \"$trim\" is not a valid argument for [-t] option. Valid arguments are \"yes\" or \"no\""
  exit 1
fi

#ensure directory containing samples folder and samples.txt exists
if [ ! -d $basedir/$seqfile/$batch ]; then
  echo -e "ERROR: Directory $basedir/$seqfile/$batch DOES NOT exist" >&2
  echo -e "Check that option arguments -b (& -s if used) is correct" >&2
  exit 1
fi

#ensure samples.txt exists
if [ ! -f $basedir/$seqfile/samples1.txt ]; then
  echo -e "ERROR: File $basedir/$seqfile/$batch/samples.txt DOES NOT exist" >&2
  echo -e "Create the standard samples.txt file for this batch" >&2
  exit 1
fi

# /home/adinasarapu/SCH10113_Samya/fastq_files/2019.1/samples.txt
# /home/adinasarapu/SCH10113_Samya/fastq_files/2019.1/samples.txt

# 0. <script_file>
# 1. batch = 2019.1 
# 2. targeted = seqfile = targetedRNA
# 3. no (trimmed)

# targetedRNA/star_2pass
# pbsfile=targetedRNA/star_2pass/star_2pass.pbs
# fastq_dir=targetedRNA/fastq_files

# mkdir -p $2/{star_2pass,psi,splicing,variant_calling}

# sample  sampledir       readgroup       fastqname
# 0: star_2pass_run 
# 1: SM04-YANIZ 
# 2: /home/adinasarapu/SCH10113_Samya/targetedRNA
# 3: YANIZ
# star_2pass_run SM04-YANIZ /home/adinasarapu/SCH10113_Samya/targetedRNA YANIZ

#star_2pass_run SM04-YANIZ /home/adinasarapu/SCH10113_Samya/targetedRNA/fastq_files YANIZ

star_2pass_run () { 
	local sample=$1
	local dir=$2
	local readgroup=$3
	mkdir -p $dir/$sample/star_2pass #STAR needs outFileNamePrefix to already exist before running
	local step=star_2pass
	local pbsfile=$dir/$sample/$step/${step}.pbs
	fastq_dir=$dir/$fastq_type
	echo "#!/bin/bash" > $pbsfile
        # job name
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
        # start pbs file for star run
	echo -e "$slrumhead_big" >> $pbsfile
	echo "module load STAR/2.7.1a" >> $pbsfile
	echo "module load samtools/1.9" >> $pbsfile
	echo "STAR --runThreadN 16 \
		--genomeDir $genomeDir \
		--sjdbGTFfile $annotations \
		--readFilesIn $fastq_dir/${fastqname}_R1_001.fastq.gz $fastq_dir/${fastqname}_R2_001.fastq.gz \
		--readFilesCommand zcat \
		--outFileNamePrefix $dir/$sample/star_2pass/ \
		--sjdbOverhang 100 \
		--outSAMtype BAM SortedByCoordinate \
		--outSAMattrRGline ID:${readgroup} PL:illumina PU:${readgroup} LB:${seqtype} SM:${sample} \
		--alignSJoverhangMin 8 \
		--alignSJDBoverhangMin 1 \
		--outFilterMultimapNmax 1 \
		--outSJfilterCountUniqueMin 5 5 5 5 \
		--outSJfilterCountTotalMin 5 5 5 5 \
		--outFilterType BySJout \
		--outSJfilterReads Unique \
		--quantMode GeneCounts \
		--twopassMode Basic" >> $pbsfile #add commands for star run
	echo -e "mv $dir/$sample/star_2pass/Aligned.sortedByCoord.out.bam $dir/$sample/star_2pass/${sample}.bam" >> $pbsfile
	echo -e "mv $dir/$sample/star_2pass/Log.final.out $dir/$sample/star_2pass/${sample}.log.final.out" >> $pbsfile
	echo -e "mv $dir/$sample/star_2pass/SJ.out.tab $dir/$sample/star_2pass/${sample}.sj.out.tab" >> $pbsfile
	echo -e "samtools index $dir/$sample/star_2pass/${sample}.bam" >> $pbsfile
	#echo -e "sbatch $dir/star_2pass/counts.pbs\n" >> $pbsfile
	#echo -e "sbatch $dir/star_2pass/qorts.pbs\n" >> $pbsfile
	#echo -e "cd $dir/psi/" >> $pbsfile 
	#echo -e "sbatch $dir/psi/psi.pbs" >> $pbsfile
	#echo -e "cd $dir/splicing/" >> $pbsfile 
	#echo -e "sbatch $dir/splicing/removedups.pbs" >> $pbsfile
	#echo -e "cd $dir/variant_calling/" >> $pbsfile #move into variant_calling dir so pbs outfile goes to here
	#echo -e "sbatch $dir/variant_calling/markdups.pbs\n" >> $pbsfile #submit next pbs file in chain
        echo "module unload samtools/1.9" >> $pbsfile
	echo "module unload STAR/2.7.1a" >> $pbsfile
}

# 0: readcounts 
# 1. SM04-YANIZ 
# 2. /home/adinasarapu/SCH10113_Samya/targetedRNA

# genecounts_script 
# geneinfo=~/data/target_coordinates/nmd274genes_gtf106_ensg.txt

readcounts () {
	local sample=$1
	local dir=$2
	local step=counts
	mkdir -p $dir/$sample/genecounts
	local pbsfile=$dir/$sample/star_2pass/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	 echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_little" >> $pbsfile #start pbs file for step
	echo -e "$genecounts_script $sample $dir/$sample $geneinfo" >> $pbsfile
	echo -e "ontarget=\`awk '{sum += \$2} END {print sum}' $dir/$sample/genecounts/${sample}.274genes.ncbi.txt\`; total=\`head -n 9 $dir/$sample/star_2pass/${sample}.log.final.out | tail -n 1 | awk '{print \$6}'\`; awk -v tot=\"\$total\" -v ont=\"\$ontarget\" 'BEGIN{print \"Reads on target: \"ont/tot}'" >> $pbsfile
}

# 0: qorts 
# 1: SM04-YANIZ 
# 2: /home/adinasarapu/SCH10113_Samya/targetedRNA 
# 3: YANIZ

qorts () {
	local sample=$1
	local dir=$2
	local readgroup=$3
	local step=qorts
	mkdir $basedir/$seqfile/metrics/qorts/${sample}.${readgroup}
	#mkdir $basedir/$seqfile/metrics/qorts/${sample}.${readgroup}.2
	#mkdir $basedir/$seqfile/metrics/qorts/${sample}.${readgroup}.3
	#mkdir $basedir/$seqfile/metrics/qorts/${sample}.${readgroup}.4
	local pbsfile=$dir/$sample/star_2pass/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
	echo -e "java -jar $qorts QC \
		--verbose \
		--stranded \
		--readGroup $readgroup $dir/$sample/star_2pass/${sample}.bam $annotations $basedir/$seqfile/metrics/qorts/${sample}.${readgroup}/" >> $pbsfile
	#echo -e "java -jar $qorts QC --verbose --stranded --readGroup $readgroup.2 $dir/star_2pass/$sample.bam $annotations $basedir/$seqfile/metrics/qorts/$sample.$readgroup.2/" >> $pbsfile
	#echo -e "java -jar $qorts QC --verbose --stranded --readGroup $readgroup.3 $dir/star_2pass/$sample.bam $annotations $basedir/$seqfile/metrics/qorts/$sample.$readgroup.3/" >> $pbsfile
	#echo -e "java -jar $qorts QC --verbose --stranded --readGroup $readgroup.4 $dir/star_2pass/$sample.bam $annotations $basedir/$seqfile/metrics/qorts/$sample.$readgroup.4/" >> $pbsfile
}

# CALCULATION OF PERCENT SPLICED IN FOR ANNOTATED EXONS (PSI)
# 1. SM04-YANIZ 
# 2. /home/adinasarapu/SCH10113_Samya/targetedRNA

psi () {
        local sample=$1
        local dir=$2
        local step=psi
        mkdir -p $dir/$sample/psi
        local pbsfile=$dir/$sample/psi/${step}.pbs
        echo "#!/bin/bash" > $pbsfile
        echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
        echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
        echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
        #echo "module load bedtools/2.25.0" >> $pbsfile
        echo -e "$psi_script \
                -d $dir/$sample \
                -s $sample \
                -l $readLength" >> $pbsfile
        echo -e "sed -i \"1i #\t#\t$sample\t$sample\t$sample\" $dir/$sample/psi/${sample}.exonic_parts.psi" >> $pbsfile
	# "exon_ID" , "length" , "inclusion" , "exclusion" , "PSI"
        #echo "module unload bedtools/2.25.0" >> $pbsfile
}


# 0: remove_dups 
# 1: SM04-YANIZ 
# 3: /home/adinasarapu/SCH10113_Samya/targetedRNA
remove_dups () {
	local sample=$1
	local dir=$2
	local step=removedups
	mkdir -p $dir/$sample/splicing
	local pbsfile=$dir/$sample/splicing/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
	echo "module load samtools/1.9" >> $pbsfile
	echo "module load picardtools/2.6.0" >> $pbsfile
	echo -e "\$PICARD MarkDuplicates \
		I=$dir/$sample/star_2pass/${sample}.bam \
		O=$dir/$sample/splicing/${sample}.nodups.bam \
		CREATE_INDEX=true \
		VALIDATION_STRINGENCY=SILENT \
		REMOVE_DUPLICATES=true \
		REMOVE_SEQUENCING_DUPLICATES=true \
		M=$dir/$sample/splicing/removedups_metrics.txt" >> $pbsfile
	echo -e "samtools view -h -u -f 0x2 $dir/$sample/splicing/${sample}.nodups.bam | \
		samtools sort -n - | \
		samtools fastq -1 $dir/$sample/splicing/${sample}.nodups.R1.fastq -2 $dir/$sample/splicing/${sample}.nodups.R2.fastq -" >> $pbsfile
	echo -e "gzip $dir/$sample/splicing/${sample}.nodups.R1.fastq" >> $pbsfile
	echo -e "gzip $dir/$sample/splicing/${sample}.nodups.R2.fastq" >> $pbsfile
	# echo -e "cd $dir/splicing/" >> $pbsfile
	echo "module unload picardtools/2.6.0" >> $pbsfile
	echo "module unload samtools/1.9" >> $pbsfile
	#echo -e "sbatch $dir/splicing/star_remap.pbs" >> $pbsfile
}

# 0: splicing_remap 
# 1: SM04-YANIZ 
# 2: /home/adinasarapu/SCH10113_Samya/targetedRNA
splicing_remap () {
	local sample=$1
	local dir=$2
	local step=star_remap
	local pbsfile=$dir/$sample/splicing/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for star run
	echo "module load STAR/2.7.1a" >> $pbsfile
	echo "module load samtools/1.9" >> $pbsfile
	echo "STAR --runThreadN 16 \
		--genomeDir $genomeDir \
		--sjdbGTFfile $annotations \
		--readFilesIn $dir/$sample/splicing/${sample}.nodups.R1.fastq.gz $dir/$sample/splicing/${sample}.nodups.R2.fastq.gz \
		--readFilesCommand zcat \
		--outFileNamePrefix $dir/$sample/splicing/ \
		--sjdbOverhang 100 \
		--outSAMtype BAM SortedByCoordinate \
		--alignSJoverhangMin 8 \
		--alignSJDBoverhangMin 1 \
		--outFilterMultimapNmax 1 \
		--outSJfilterCountUniqueMin 5 5 5 5 \
		--outSJfilterCountTotalMin 5 5 5 5 \
		--outFilterType BySJout \
		--outSJfilterReads Unique \
		--quantMode GeneCounts \
		--twopassMode Basic" >> $pbsfile #add commands for star run
	echo -e "mv $dir/$sample/splicing/Aligned.sortedByCoord.out.bam $dir/$sample/splicing/${sample}.remap.nodups.bam" >> $pbsfile
	echo -e "samtools index $dir/$sample/splicing/${sample}.remap.nodups.bam" >> $pbsfile
	echo -e "mv $dir/$sample/splicing/Log.final.out $dir/$sample/splicing/${sample}.nodups.log.final.out" >> $pbsfile
	echo -e "mv $dir/$sample/splicing/SJ.out.tab $dir/$sample/splicing/${sample}.nodups.sj.out.tab" >> $pbsfile
	echo "module unload STAR/2.7.1a" >> $pbsfile
	echo "module unload samtools/1.9" >> $pbsfile
	#echo -e "cd $dir/splicing/" >> $pbsfile
	#echo -e "sbatch $dir/splicing/splice_counts.pbs" >> $pbsfile
	#echo -e "sbatch $dir/splicing/counts_nodups.pbs" >> $pbsfile
}

# 0: readcounts_nodups 
# 1: SM04-YANIZ 
# 2: /home/adinasarapu/SCH10113_Samya/targetedRNA
readcounts_nodups () {
	local sample=$1
	local dir=$2
	local step=counts_nodups
	local pbsfile=$dir/$sample/splicing/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_little" >> $pbsfile #start pbs file for step
	echo -e "$genecounts_nodupsscript $sample $dir/$sample $geneinfo" >> $pbsfile
	echo -e "ontarget=\`awk '{sum += \$2} END {print sum}' $dir/$sample/genecounts/${sample}.nodups.274genes.ncbi.txt\`; total=\`head -n 9 $dir/$sample/splicing/${sample}.nodups.log.final.out | tail -n 1 | awk '{print \$6}'\`; awk -v tot=\"\$total\" -v ont=\"\$ontarget\" 'BEGIN{print \"Reads on target: \"ont/tot}'" >> $pbsfile
}

# 0: norm_label_splices 
# 1: SM04-YANIZ 
# 2: /home/adinasarapu/SCH10113_Samya/targetedRNA
norm_label_splices () {
	local sample=$1
	local dir=$2
	local step=splice_counts
	local pbsfile=$dir/$sample/splicing/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_little" >> $pbsfile #start pbs file for star run
	echo -e "$ind_splicing $dir/$sample $sample $sortedintervals $introns" >> $pbsfile
}

###################
# Variant Calling #
###################
# 1: mark_duplicates 
mark_duplicates () {
	local sample=$1
	local dir=$2
	local step=markdups
	mkdir -p $dir/$sample/variant_calling
	mkdir $dir/$sample/variant_calling/temp
	local pbsfile=$dir/$sample/variant_calling/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
	echo "module load picardtools/2.6.0" >> $pbsfile
	echo -e "\$PICARD MarkDuplicates \
		I=$dir/$sample/star_2pass/${sample}.bam \
		O=$dir/$sample/variant_calling/temp/${sample}.dedup.bam \
		CREATE_INDEX=true \
		VALIDATION_STRINGENCY=SILENT \
		REMOVE_DUPLICATES=false \
		REMOVE_SEQUENCING_DUPLICATES=true \
		M=$dir/$sample/variant_calling/markdups_metrics.txt" >> $pbsfile
	# echo -e "cd $dir/variant_calling/" >> $pbsfile #move into variant_calling dir so pbs outfile goes to here
	echo "module unload picardtools/2.6.0" >> $pbsfile
	# echo -e "sbatch $dir/variant_calling/splitn.pbs\n" >> $pbsfile
}

# 2: split_reads 
split_reads () {
	local sample=$1
	local dir=$2
	local step=splitn
	local pbsfile=$dir/$sample/variant_calling/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
	# No more need "-rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS" in GATK4
	echo -e "java -jar $gatk SplitNCigarReads \
		-R $wholeGenomeFasta \
		-I $dir/$sample/variant_calling/temp/${sample}.dedup.bam \
		-O $dir/$sample/variant_calling/temp/${sample}.dedup.split.bam" >> $pbsfile
	# echo -e "cd $dir/variant_calling/" >> $pbsfile #move into variant_calling dir so pbs outfile goes to here
	# echo -e "sbatch $dir/variant_calling/baserecal.pbs\n" >> $pbsfile
}

# 3: base_recalibration 
base_recalibration () {
	local sample=$1
	local dir=$2
	local step=baserecal
	local pbsfile=$dir/$sample/variant_calling/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
	echo -e "java -jar $gatk BaseRecalibrator \
		-R $wholeGenomeFasta \
		-I $dir/$sample/variant_calling/temp/${sample}.dedup.split.bam \
		--known-sites $phase1snps \
		--known-sites $millsIndels \
		--known-sites $dbsnp \
		-O $dir/$sample/variant_calling/recal_data.table" >> $pbsfile
	echo -e "java -jar $gatk ApplyBQSR \
		-R $wholeGenomeFasta \
		-bqsr $dir/$sample/variant_calling/recal_data.table \
		-I $dir/$sample/variant_calling/temp/${sample}.dedup.split.bam \
		-O $dir/$sample/variant_calling/temp/${sample}.dedup.split.recal.bam" >> $pbsfile
	#echo -e "java -jar $gatk PrintReads -R $wholeGenomeFasta -I $dir/$sample/variant_calling/temp/${sample}.dedup.split.bam -BQSR $dir/$sample/variant_calling/recal_data.table -O $dir/$sample/variant_calling/temp/${sample}.dedup.split.recal.bam" >> $pbsfile
	# echo -e "cd $dir/variant_calling/" >> $pbsfile #move into variant_calling dir so pbs outfile goes to here
	# echo -e "sbatch $dir/variant_calling/vcf.pbs\n" >> $pbsfile
}

# 4: vcf 
vcf () {
	local sample=$1
	local dir=$2
	local step=vcf
	local pbsfile=$dir/$sample/variant_calling/${step}.pbs
	echo "#!/bin/bash" > $pbsfile
	echo -e "#SBATCH --job-name=${readgroup}.${step}" >> $pbsfile
	echo -e "#SBATCH --output=${readgroup}.${step}.log" >> $pbsfile
	echo -e "$slrumhead_big" >> $pbsfile #start pbs file for step
	# -L $sortedintervals
	echo -e "java -jar $gatk HaplotypeCaller \
		-R $wholeGenomeFasta \
		-I $dir/$sample/variant_calling/temp/${sample}.dedup.split.recal.bam \
		--dbsnp $dbsnp \
		--kmer-size $kmersize \
		--dont-use-soft-clipped-bases true \
		-stand-call-conf 20.0 \
		-O $dir/$sample/variant_calling/${sample}.nodups.raw.snps.indels.vcf" >> $pbsfile
	# The window size (in bases) in which to evaluate clustered SNPs  Default value: 0.
	# cluster: The number of SNPs which make up a cluster. Must be at least 2  Default value: 3.
	echo -e "java -jar $gatk VariantFiltration \
		-R $wholeGenomeFasta \
		-V $dir/$sample/variant_calling/${sample}.nodups.raw.snps.indels.vcf \
		-window 35 \
		-cluster 3 \
		--filter-name FS -filter \"FS > 30.0\" \
		--filter-name QD -filter \"QD < 2.0\" \
		--filter-name DP -filter \"DP < 11.0\" \
		-G-filter-name GQ -G-filter \"GQ < 5.0\" \
		-G-filter-name HET -G-filter \"isHet == 1 && DP > 10\" \
		-O $dir/$sample/variant_calling/${sample}.nodups.filtered.vcf" >> $pbsfile
	echo -e "$annovar $dir/$sample/variant_calling/${sample}.nodups.filtered.vcf ~/humandb/ \
		-buildver hg38 \
		-out $dir/$sample/variant_calling/${sample}.nodups.filtered.annotated.vcf \
		-remove \
		-protocol refGene,cytoBand,genomicSuperDups,avsnp150,dbnsfp35c,clinvar_20180603,exac03,gnomad_exome \
		-operation g,r,r,f,f,f,f,f \
		-argument '-hgvs,-hgvs,-hgvs,-hgvs,-hgvs,-hgvs,-hgvs,-hgvs' \
		-nastring . -vcfinput" >> $pbsfile
}

mkdir -p $basedir/$seqfile/metrics/qorts
awk -F: '/^[^#]/ { print $0 }' $basedir/$seqfile/samples1.txt | while read sample sampledir readgroup fastqname
do
	dir=$basedir/$seqfile
	star_2pass_run $sample $dir $readgroup
	readcounts $sample $dir
	qorts $sample $dir $readgroup
	psi $sample $dir
	remove_dups $sample $dir
	splicing_remap $sample $dir
	norm_label_splices $sample $dir
	readcounts_nodups $sample $dir
	mark_duplicates $sample $dir
	split_reads $sample $dir
	base_recalibration $sample $dir
	vcf $sample $dir

	# 1. star_2pass_run
	id0=""
	echo "star_2pass submitted for $sample ...$id0"
	id0=`sbatch $dir/$sample/star_2pass/star_2pass.pbs`

        ######################################
        # gene counts - with duplicate reads #
        ######################################

	# 2. readcounts (needs geneinfo="nmd274genes_gtf106_ensg.txt" file)
  	id0=${id0##* }
	echo "counts submitted for $sample ...$id0"
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/star_2pass/counts.pbs`
      	id0=${id0##* }

	##############
	# mapping qc #
	##############

	# 3. qorts
	echo "qorts submitted for $sample ...$id0"
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/star_2pass/qorts.pbs`
	id0=${id0##* }
	
	######################
	# Alternate splicing #
	######################

	# 4. psi (needs exonic_parts gff file)
	echo "psi submitted for $sample ..."
        id0=`sbatch --dependency=afterany:${id0} $dir/$sample/psi/psi.pbs`
	id0=${id0##* }

	# 5. remove_dups
	echo "removedups submitted for $sample ...$id0"
        id0=`sbatch --dependency=afterany:${id0} $dir/$sample/splicing/removedups.pbs`
	id0=${id0##* }
	
	# 6. splicing_remap
	echo "star_remap submitted for $sample ...$id0"
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/splicing/star_remap.pbs`
	id0=${id0##* }

	# 7. norm_label_splices (needs sorted_intervals and introns files)
	echo "splice_counts submitted for $sample ..."
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/splicing/splice_counts.pbs`
	id0=${id0##* }

	#########################################
	# gene counts - without duplicate reads #
	#########################################

	# 8. readcounts_nodups (needs nmd274genes_gtf106_ensg.txt file)
	echo "counts_nodups submitted for $sample ..."
        id0=`sbatch --dependency=afterany:${id0} $dir/$sample/splicing/counts_nodups.pbs`
	id0=${id0##* }

	###################
	# Variant calling #
	###################

	# 9. mark_duplicates
	echo "markdups submitted for $sample ...$id0"
        id0=`sbatch --dependency=afterany:${id0} $dir/$sample/variant_calling/markdups.pbs`
	id0=${id0##* }

	# 10. split_reads
	echo "splitn submitted for $sample ...$id0"
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/variant_calling/splitn.pbs`
	id0=${id0##* }
	
	# 11. base_recalibration 
	echo "baserecal submitted for $sample ...$id0"
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/variant_calling/baserecal.pbs`
	id0=${id0##* }

	# 12. vcf
	id0=`sbatch --dependency=afterany:${id0} $dir/$sample/variant_calling/vcf.pbs`
	id0=${id0##* }
	echo "vcf submitted for $sample ...$id0"
done
