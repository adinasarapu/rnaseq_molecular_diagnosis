#!/bin/bash

# ls /mnt/beegfs/v1/eicc/sw/eicc/Data/Illumina/Homo_sapiens/NCBI/GRCh38/Annotation/Genes/genes.gtf

basedir=$HOME/SCH10113_Samya
# tools=~/data/tools
proj_dir=$basedir/rnaseq-nmd

human_genome=/mnt/beegfs/v1/eicc/sw/eicc/Data/Illumina
genomeDir=$basedir/genomeDir/illumina_grch38

#wholeGenomeFasta=$proj_dir/data/genome.fa
#annotations=$proj_dir/data/genes.gtf

wholeGenomeFasta=$human_genome/Homo_sapiens/NCBI/GRCh38/Sequence/WholeGenomeFasta/genome.fa
annotations=$human_genome/Homo_sapiens/NCBI/GRCh38/Annotation/Genes/genes.gtf

# gene_symbol, ens_gene_id, gene_length
# hgnc_id	ensg_id	length
# ABCA1	ENSG00000165029	11672
# ABCD1	ENSG00000101986	3654
# ABHD5	ENSG00000011198	5346
# ...
geneinfo=$proj_dir/target_coordinates/nmd274genes_gtf106_ensg.txt

# https://github.com/olgabot/rna-seq-diff-exprn/blob/master/scripts/external/dexseq_prepare_annotation.py
# python dexseq_prepare_annotation.py gtffile.gtf dexseq.gff
#chr1	dexseq_prepare_annotation.py	exonic_part	1020123	1020373	.	+	.	AGRN:001
#chr1	dexseq_prepare_annotation.py	exonic_part	1022201	1022462	.	+	.	AGRN:002
#chr1	dexseq_prepare_annotation.py	exonic_part	1035277	1035324	.	+	.	AGRN:003
exonparts=$proj_dir/target_coordinates/ncbi274.exonic_parts.forPSI.gff

# gene_symbol, strand, chr, start, stop 
sortedintervals=$proj_dir/target_coordinates/nmd274genes_intervals_vsort.txt
# gene_symbol, intron_transcript, intron_type, intron_strand, intron_chr, intron_start, intron_stop
introns=$proj_dir/target_coordinates/condensed_introns_nooverlap.txt

# sortedintervals=~/data/target_coordinates/nmd274genes_intervals_vsort.txt
# geneinfo=~/data/target_coordinates/nmd274genes_gtf106_ensg.txt
# introns=~/data/target_coordinates/condensed_introns_nooverlap.txt

# STAR=/mnt/beegfs/v1/eicc/sw/eicc/Pkgs/STAR/2.7.1a/bin
qorts=/home/adinasarapu/QoRTs/QoRTs.jar
picard=/mnt/beegfs/v1/eicc/sw/eicc/Pkgs/picardtools/2.6.0/picard.jar
# bedtools=/mnt/beegfs/v1/eicc/sw/eicc/Pkgs/bedtools/2.25.0/bin

gatk=/home/adinasarapu/gatk-4.1.2.0/gatk-package-4.1.2.0-local.jar
# unzip
phase1snps=$HOME/gatk-4.1.2.0/bundle/ftp.broadinstitute.org/bundle/hg38/1000G_phase1.snps.high_confidence.hg38.vcf.gz
millsIndels=$HOME/gatk-4.1.2.0/bundle/ftp.broadinstitute.org/bundle/hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
dbsnp=$HOME/gatk-4.1.2.0/bundle/ftp.broadinstitute.org/bundle/hg38/dbsnp_146.hg38.vcf.gz

# annotate_variation.pl -downdb -webfrom annovar avdblist ~/humandb/ -buildver hg38
# annotate_variation.pl -buildver hg38 -downdb -webfrom annovar avsnp150 ~/humandb/
annovar=/mnt/beegfs/v1/eicc/sw/eicc/Pkgs/annovar/201804/bin/table_annovar.pl

genecounts_script=$proj_dir/scripts/genecounts_nmdgenes.pl
genecounts_nodupsscript=$proj_dir/scripts/genecounts.nodups.nmdgenes.pl
psi_script=$proj_dir/scripts/calc_psi.sh
ind_splicing=$proj_dir/scripts/nmdgenes_sjs.pl

# sbatch 
slrumhead_big="#SBATCH --cpus-per-task=20\n#SBATCH --mail-type=ALL\n#SBATCH --mail-user=ashok.reddy.dinasarapu@emory.edu" 
slrumhead_little="#SBATCH --cpus-per-task=4\n#SBATCH --mail-type=ALL\n#SBATCH --mail-user=ashok.reddy.dinasarapu@emory.edu"

# pbshead_big="#PBS -l nodes=1:ppn=12\n#PBS -l mem=40gb\n#PBS -l walltime=12:00:00\n#PBS -q iw-shared-6\n#PBS -j oe\n\ncd \$PBS_O_WORKDIR"
# pbshead_little="#PBS -l nodes=1:ppn=4\n#PBS -l mem=8gb\n#PBS -l walltime=12:00:00\n#PBS -q iw-shared-6\n#PBS -j oe\n\ncd \$PBS_O_WORKDIR"
