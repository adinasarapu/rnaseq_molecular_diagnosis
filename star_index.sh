#!/bin/sh

#SBATCH --job-name=STAR_IDX

# job submit as
# sbatch --cpus-per-task=15 star_index.sh

PROJ_DIR=$HOME/SCH10113_Samya

echo "Start - `date`"

module load STAR/2.7.1a

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMPDIR
/usr/bin/mkdir -p $TMPDIR/genomeDir/illumina_grch38
/usr/bin/mkdir -p $TMPDIR/ref

rsync -av /mnt/beegfs/v1/eicc/sw/eicc/Data/Illumina/Homo_sapiens/NCBI/GRCh38/Sequence/WholeGenomeFasta/genome.fa $TMPDIR/ref/
rsync -av /mnt/beegfs/v1/eicc/sw/eicc/Data/Illumina/Homo_sapiens/NCBI/GRCh38/Annotation/Genes/genes.gtf $TMPDIR/ref/

# STAR indexing of ref genome
# --genomeDir path to the directory where genome files are stored

STAR --runMode genomeGenerate \
 --genomeDir $TMPDIR/genomeDir/illumina_grch38 \
 --genomeFastaFiles $TMPDIR/ref/genome.fa \
 --outTmpDir $TMPDIR/idx.tmp \
 --runThreadN 12

rsync -avr $TMPDIR/genomeDir $PROJ_DIR

rm -rf $TMPDIR

module unload STAR/2.7.1a

echo "END - `date`"
