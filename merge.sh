#!/bin/sh

#SBATCH --job-name=merge

# job submit as
# sbatch --cpus-per-task=1 merge.sh

PROJ_DIR=$HOME/SCH10113_Samya
SEQ_DIR=$PROJ_DIR/test_samples

mkdir -p $PROJ_DIR/test_samples_merged

FASTQ_DIR=$PROJ_DIR/test_samples_merged

# get sample IDs from file names
list=$(ls $SEQ_DIR/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_L"}{ print $1 }' | sort | uniq)

echo $list

for file in $list; do
 echo "ID --> ${file}"
 for j in 1 2; do
  # merge lane data
  Rall=$SEQ_DIR/${file}_L00[1,2,3,4]_R${j}_001.fastq.gz 
  Rcat=$FASTQ_DIR/${file}_R${j}_001.fastq.gz
  zcat $Rall | gzip -c > $Rcat
 done
done
