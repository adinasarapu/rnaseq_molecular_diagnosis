# RNA-seq molecular diagnosis  

A novel three-tiered approach of targeted RNA-seq analysis for molecular diagnosis of a genetic disease (ex. neuromuscular disease; NMD) was proposed. Analysis will be stopped if molecular diagnosis is achieved in any of the Tiers and the results will be clinically correlated to reclassify variants of uncertain significance (VUSs), identify pathogenic events at the mRNA level, and understand the nature pathogenicity of the variants.  

*Tier 1 analyses includes*    
i). Aberrant splicing events including exon(s) skip(s), pseudo-exon gain, exon extension or exon splice gain evaluated.  
ii). RNA-seq based variant calling including confirmation of specific variants found previously in DNA-seq, identification of novel variants not found in DNA-seq, or observation of transcript structure effects of DNA variants evaluated.  
iii). Allele expression imbalance (AEI) including that resulted due to non-sense mediated decay, non-stop mediated decay, or allele-specific expression evaluated.  

*Tier 2 analyses include*.   
Differential isoform pattern or abundance including differential exon usage or percent spliced in evaluated.  

*Tier 3 analyses include*.  
Quantification of transcript abundance (gene expression) of marker gene, and then if required the rest of genes in panel evaluated.  

Scripts are available for run on [Computing Cluster](https://bitbucket.org/adinasarapu/rnaseq_molecular_diagnosis/src) using Slurm workload manager.  

References:  

1. Gonorazky HD, Naumenko S, Ramani AK, et al. Expanding the boundaries of RNA sequencing as a diagnostic tool for rare Mendelian disease. Am J Hum Genet. 2019;104:466–483.  
2. [VUS](https://www.cancer.gov/publications/dictionaries/genetics-dictionary?cdrid=556493) - `A variation in a genetic sequence whose association with disease risk is unknown. Also called unclassified variant, variant of uncertain significance and VUS.’  
3. [PSI](https://www.ncbi.nlm.nih.gov/pubmed/26439713) - percent spliced in