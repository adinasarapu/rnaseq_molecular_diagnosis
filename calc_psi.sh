#!/bin/bash

# Percent Spliced-in Index (PSI): 

# A PSI of 100% indicates constitutive exons that are included in all transcripts and never removed from expressed isoforms.
# The PSI is based on inclusion reads (IR) and exclusion reads (ER)
# IRs overlap with the exonic features 
# ERs are split alignments
# Diffrences in PSI values between conditions can be uased to filter differential exon or isoform expression.

# Citation: Curr Protoc Hum Genet. 2015 Oct 6;87:11.16.1-14

# -s SM04-YANIZ 
# -d /home/adinasarapu/SCH10113_Samya/targetedRNA/SM04-YANIZ
# -l 151

scriptname=$0   # $0 is the name of the program
sampledir=""
sample=""
readLength=151
vars=$HOME/SCH10113_Samya/rnaseq-nmd/scripts/variableslist.sh

#help function
HELP () {
	echo -e "\n\tUSAGE: $scriptname -d DIR -s SAMPLEID [-l READ_LENGTH] [-v VARIABLES_FILE]\n"
	echo -e "\tMANDATORY OPTIONS:\n"
	echo -e "\t\t-d DIR\t\tAbsolute Path to the sample directory\n"
	echo -e "\t\t-s SAMPLEID\tSample ID used in file names\n\n"
	echo -e "\tADDITIONAL OPTIONS:\n"
	echo -e "\t\t-v VARIABLES_FILE\tLocation of file containing variable information for analysis scripts"
	echo -e "\t\t\t\t\tDEFAULT: ~/data/scripts/variableslist.sh\n"
	echo -e "\t\t-l READ_LENGTH\tRead Length for sample reads. Must be a positive integer."
		echo -e "\t\t\t\t\tDEFAULT: 151\n"
	exit 0
}

#Check the number of arguments. If none are passed, print help and exit.
NUMARGS=$#
if [ $NUMARGS -eq 0 ]; then
  HELP
fi

#Get Options from command line
while getopts :s:d:l:v:h opt
do
	case $opt in
	s)sample=$OPTARG;;
	d)sampledir=$OPTARG;;
	l)readLength=$OPTARG;;
	v)vars=$OPTARG;;
    h) HELP; exit;;
    \?) echo "ERROR: Invalid option: -$OPTARG" >&2; echo -e "Run $scriptname -h to see usage and help" >&2; exit 1;;
    :) echo "ERROR: Option -$OPTARG requires an argument" >&2; echo -e "Run $scriptname -h to see usage and help" >&2; exit 1;;
    *) echo "ERROR: Unimplemented option: -$OPTARG" >&2; echo -e "Run $scriptname -h to see usage and help" >&2; exit 1;;
  esac
done
shift $(($OPTIND -1))

#check to make sure variables file exists
if [ ! -f $vars ]; then
  echo -e "ERROR: File $vars DOES NOT exist" >&2
  echo -e "Edit this scripts default or use the -v option to point the script to your file that contains variables for analysis scripts" >&2
  exit 1
fi

source $vars

#make sure read length is a positive integer
if ! [[ "$readLength" =~ ^[0-9]+$ ]]; then
  echo -e "ERROR: \"$readLength\" is not a valid argument for [-l] option. Read Length must be an integer."
  exit 1
fi

# module load bedtools/2.25.0 

# sj.out.tab contains high confidence collapsed splice junctions in tab-delimited format. 

# 1. chromosome
# 2. first base of the intron (1-based)
# 3. last base of the intron (1-based)
# 4. strand (0: undefined, 1: +, 2: -)
# 5. intron motif: 0: non-canonical; 1: GT/AG, 2: CT/AC, 3: GC/AG, 4: CT/GC, 5: AT/AC, 6: GT/AT
# 6. 0: unannotated, 1: annotated (only if splice junctions database is used) 
# 7. number of uniquely mapping reads crossing the junction
# 8. number of multi-mapping reads crossing the junction
# 9. maximum spliced alignment overhang

# before
# chr1	827776	829002	1	1	1	1	0	62
# chr1	952140	952411	2	2	1	1	0	73
# chr1	976270	976498	2	2	1	1	0	44
# chr1	1020374	1022200	1	1	1	18	0	73
# chr1	1041398	1041477	1	1	1	1	0	63
# after
# chr1	827755	829022	JUNCBJ1	1	+	827755	829022	255,0,0	2	20,20	0,300
# chr1	952119	952431	JUNCBJ2	1	-	952119	952431	255,0,0	2	20,20	0,300
# chr1	976249	976518	JUNCBJ3	1	-	976249	976518	255,0,0	2	20,20	0,300
# chr1	1020353	1022220	JUNCBJ4	18	+	1020353	1022220	255,0,0	2	20,20	0,300
# chr1	1041377	1041497	JUNCBJ5	1	+	1041377	1041497	255,0,0	2	20,20	0,300
# chr1	1041682	1041975	JUNCBJ6	4	+	1041682	1041975	255,0,0	2	20,20	0,300



#put sj.out.tab into correct bed format for use
awk 'BEGIN{OFS="\t"}{print $1, $2-20-1, $3+20, "JUNCBJ"NR, $7,
($4 == 1)? "+":"-",$2-20-1, $3+20, "255,0,0", 2, "20,20",
"0,300" }' $sampledir/star_2pass/${sample}.sj.out.tab > $sampledir/psi/${sample}.junctions.bed

# Define intronic locations w/in junction file
# Transforms the junction file to determine the genomic location of introns covered by exclusion reads
#  
sed 's/,/\t/g' $sampledir/psi/${sample}.junctions.bed |\
	 grep -v description |\
	 awk '{OFS="\t"}{print $1,$2+$13,$3-$14,$4,$5,$6}' > $sampledir/psi/${sample}.intron.bed
rm $sampledir/psi/${sample}.junctions.bed

# Determine the IR count for each exonic part.
# conda/bedtools v2.28.0
# coverage command in bedtools suite counts the number of alignments overlaping with exonic parts

# -split	Treat "split" BAM or BED12 entries as distinct BED intervals
# -s		Require same strandedness.
# -g		Provide a genome file to enforce consistent chromosome sort order across input files.
# -a <bed/gff/vcf> 
# -b <bed/gff/vcf>
bedtools coverage -split -s -sorted \
	-g ${wholeGenomeFasta}.fai \
	-a $exonparts \
	-b $sampledir/star_2pass/${sample}.bam |\
	 awk 'BEGIN{OFS="\t"}{print $1,$4,$5,$5-$4+1,$9,$10}' |\
	 sort -k 5 > $sampledir/psi/${sample}.exonic_parts.inclusion

# Count exclusion reads (ER) by intersecting introns with exonic parts and counting number of split alignments overlapping each exonic part
bedtools intersect -wao \
	-f 1.0 \
	-s -sorted \
	-g ${wholeGenomeFasta}.fai \
	-a $exonparts \
	-b $sampledir/psi/${sample}.intron.bed |\
	 awk 'BEGIN{OFS="\t"}{$16 == 0? s[$9] += 0:s[$9] += $14}END{for (i in s) {print i,s[i]}}' |\
	 sort -k 1 > $sampledir/psi/${sample}.exonic_parts.exclusion
rm $sampledir/psi/${sample}.intron.bed

# PSI calculation
# First normalize IRs and ERs
# Then calculate PSI = IR/(IR+ER)
paste $sampledir/psi/${sample}.exonic_parts.inclusion $sampledir/psi/${sample}.exonic_parts.exclusion | awk -v "len=$readLength" 'BEGIN{OFS="\t"; print "exon_ID" , "length" , "inclusion" , "exclusion" , "PSI"}{NIR=$6/($4+len-1) ; NER=$8/(len-1)}{print $5,$4,$6,$8,(NIR+NER<=0)? "NA":NIR / (NIR + NER)}' > $sampledir/psi/${sample}.exonic_parts.psi  

rm $sampledir/psi/${sample}.exonic_parts.inclusion
rm $sampledir/psi/${sample}.exonic_parts.exclusion
